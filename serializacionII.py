import pickle 

class Vehiculos():
    def __init__(self, marca, modelo):
        self.marca=marca
        self.modelo=modelo
        self.enmarcha=False
        self.acelera=False
        self.frena=False

    def arrancar(self):
        self.enmarcha=True

    def acelerar(self):
        self.acelera=True

    def frenar(self):
        self.frena=True

    def estado(self):
        print("<<------------------>>")
        print(" Marca:",self.marca,"\n Modelo:",self.modelo,"\n En marcha",self.enmarcha,"\n Acelera:",
        self.acelera,"\n Frena:",self.frena)

coche1=Vehiculos("Mazda","mds")

coche2=Vehiculos("Seat","Lr5")

coche3=Vehiculos("Renaut","Mzx")

coches=[coche1,coche2,coche3]

fichero=open("losCoches","wb")

pickle.dump(coches,fichero)

fichero.close()

del(fichero)

ficheroApertura=open("losCoches","rb")

misCoches=pickle.load(ficheroApertura)

ficheroApertura.close()

for i in misCoches:

    print(i.estado())

#Si lo hacemos en otro archivo.py tenemos que pegar toda la clase

""" 
import pickle 

class Vehiculos():
    def __init__(self, marca, modelo):
        self.marca=marca
        self.modelo=modelo
        self.enmarcha=False
        self.acelera=False
        self.frena=False

    def arrancar(self):
        self.enmarcha=True

    def acelerar(self):
        self.acelera=True

    def frenar(self):
        self.frena=True

    def estado(self):
        print("<<------------------>>")
        print(" Marca:",self.marca,"\n Modelo:",self.modelo,"\n En marcha",self.enmarcha,"\n Acelera:",
        self.acelera,"\n Frena:",self.frena)
        
ficheroApertura=open("losCoches","rb")

misCoches=pickle.load(ficheroApertura)

ficheroApertura.close()

for i in misCoches:

    print(i.estado()) 
    
                         """
