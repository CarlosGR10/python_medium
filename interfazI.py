"""
Created on Wed Sep 18 09:01:16 2019

@author: carlos
"""

from tkinter import *

raiz=Tk() # Creamos la ventana principal

raiz.title("My first window")

#raiz.resizable(1,0) #Método--- resizable(Ancho, Alto)

raiz.geometry("700x600") #Medidas de la ventana

raiz.config(bg="#F5F5F5") # backgroung de la ventana

raiz.mainloop() #Es una ventana en ejecucuión

#Notas: para que aparezca una ventana lo guardamos con una extencion
# .pyw (Python window)