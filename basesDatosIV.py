#Cláusulas UNIQUE y CRUD
import sqlite3

miConexion=sqlite3.connect("bdCRUD")

miCursor=miConexion.cursor()
#El Campo clave se declara como PRIMARY KEY
#UNIQUE evita que hayan registors repetidos

miCursor.execute('''
    CREATE TABLE PRODUCTOS(
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        NOMBRE_ARTICULO VARCHAR(50) UNIQUE,
        PRECIO INTEGER,
        SECCION VARCHAR(20))
''')

#--->>>Operación Create

productos=[
    ("Pelota",20,"Juguetes"),
    ("Pantalon",15,"Ropa"),
    ("Desarmador",25,"Ferretería"),
    ("Jarrón",50,"Cerámica")
]

miCursor.executemany("INSERT INTO PRODUCTOS VALUES (NULL,?,?,?)",productos)

#Operaciones de un CRUD

##----->>>READ
miCursor.execute("SELECT * FROM PRODUCTOS WHERE SECCION='Ropa'")

##---->>>UPDATE
miCursor.execute("UPDATE PRODUCTOS SET PRECIO=35 WHERE NOMBRE_ARTICULO = 'Pelota'")
##--->UPDATE tablaX SET CampoX where CampoY = 'ITEM'

##---->>>DELETE

miCursor.execute("DELETE FROM PRODUCTOS WHERE ID =5")

miConexion.commit()
miConexion.close()