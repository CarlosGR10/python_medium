import pickle
class Persona():
    def __init__(self, nombre, genero, edad):
        self.nombre=nombre
        self.genero=genero
        self.edad=edad
        print("Se ha creado una persona nueva con el nombre de", self.nombre)

    def __str__(self):
        return "{} {} {}".format(self.nombre, self.genero, self.edad)

class ListaPersonas():
    personas=[]

    def __init__(self):
        listaDePersonas=open("ficheroExterno","ab+") #Añadido en un modo binario
        listaDePersonas.seek(0) #Mueve el puntero hacia el byte indicado

        try:

            self.personas=pickle.load(listaDePersonas)
            print("se cargaron {} personas del fichero externo".format(len(self.personas)))

        except:
            print("El fichero esta vacio")

        finally:

            listaDePersonas.close()
            del(listaDePersonas)

    def agregarPersonas(self, p):
        self.personas.append(p)
        self.guardarPersonasEnArchivoExterno
    
    def mostrarPersonas(self):
        for p in self.personas:
            print(p)
    
    #pickle es almacenar objetos de python directamenteenun archivo sin nesecidad de una conversión
    
    def guardarPersonasEnArchivoExterno(self):
        listaDePersonas=open("ficheroExterno","wb") #Escritura binaria
        pickle.dump(self.personas, listaDePersonas) #Volcar datos
        listaDePersonas.close() 
        del(listaDePersonas)

    def mostrarInfoFicheroExterno(self):
        print("La informacion del fichero esterno es: ")
        for p in self.personas:
            print(p)

miLista=ListaPersonas()
persona=Persona("Maira","Femenino",25)
miLista.agregarPersonas(persona)
miLista.mostrarInfoFicheroExterno()