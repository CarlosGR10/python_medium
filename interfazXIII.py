from tkinter import *
from tkinter import filedialog

principal = Tk()

def abreFichero():
    fichero=filedialog.askopenfilename(title="Abrir",initialdir="c:",filetypes=(("Exel","*xlsx"),
    ("Textos","*txt"),("Todos los archivos","*.*")))
    print(fichero)
    
#Abre un fichero en el sistema operativo

Button (principal,text="Abrir fichero", command=abreFichero).pack()


principal.mainloop()