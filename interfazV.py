#Aplicacion del Grid en las interfases

from tkinter import *

ventana=Tk()

miFrame=Frame(ventana, width="100", height="100")
miFrame.pack()

minombre=StringVar() #Variable para enviar al promp

cuadroNombre=Entry(miFrame, textvariable=minombre) # Promp donde se recibira la variable
cuadroNombre.grid(row=0, column=1, padx=10, pady=10)

cuadroApellido=Entry(miFrame)
cuadroApellido.grid(row=1, column=1,padx=10, pady=10)

cuadroDireccion=Entry(miFrame)
cuadroDireccion.grid(row=2, column=1 ,padx=10, pady=10)

cuadropass=Entry(miFrame)
cuadropass.grid(row=3, column=1, padx=10, pady=10)
cuadropass.config(show="*")

# Crear un cuadro de texto con su scroll
textoObs=Text(miFrame, width=16, height=5)
textoObs.grid(row=4, column=1, padx=10, pady=10)

scroll=Scrollbar(miFrame, command=textoObs.yview)
scroll.grid(row=4, column=2, sticky="nsew")

textoObs.config(yscrollcommand=scroll.set)
# Aqui termina el TEXT
#sticky(adhesivo) sirve para alinear

nombreLabel=Label(miFrame, text="Nombre")
nombreLabel.grid(row=0, column=0, pady=10, sticky="e")

apellidoLabel=Label(miFrame, text="Apellido")
apellidoLabel.grid(row=1, column=0, pady=10, sticky="e")

direccionLabel=Label(miFrame, text="Direccion")
direccionLabel.grid(row=2, column=0, pady=10, sticky="e")

passLabel=Label(miFrame, text="Password")
passLabel.grid(row=3, column=0, pady=10, sticky="e")

obsLabel=Label(miFrame, text="Observaciones")
obsLabel.grid(row=4, column=0, pady=10, sticky="e")

# Enviar informacion a un campo de texto
def codigoBoton():

    minombre.set("Carlos") #El texto a enviar al promp

# Crear un boton    
botonE=Button(miFrame, text="Envio", command=codigoBoton)
botonE.grid(row=5, column=1)

ventana.mainloop()