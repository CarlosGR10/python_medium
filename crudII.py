from tkinter import *
from tkinter import messagebox
import sqlite3

#-----------Funciones-------------------

def conexionBBDD():
    miConexion=sqlite3.connect("Usuarios")
    miCursor=miConexion.cursor()

    try:

        miCursor.execute('''
        CREATE TABLE DATOSUSUARIOS(
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            NOMBRE_USUARIO VARCHAR(50),
            PASSWORD VARCHAR(50),
            APELLIDO VARCHAR(10),
            DIRECCION VARCHAR(50),
            COMENTARIOS VARCHAR(100))
            ''')

        messagebox.showinfo("BBDD","BBDD creada con exito")

    except:

        messagebox.showwarning("!Atencion¡","La base de datos ya existe")

def salirAplicacion():

    valor=messagebox.askquestion("Salir","Quieres salir de la aplicación")

    if(valor == 'yes'):
        principal.destroy()

def limpiarDatos():

    miNombre.set("")
    miId.set("")
    miApellido.set("")
    Midireccion.set("")
    miPass.set("")
    textoComentario.delete(1.0, END) # Debe de borrar desde el primer caracter hasta el final

#------------------CRUD--------------------------------
def crear():

    miConexion=sqlite3.connect("Usuarios")

    miCursor=miConexion.cursor()

    datos=miNombre.get(),miPass.get(),miApellido.get(),Midireccion.get(),textoComentario.get("1.0", END)
    miCursor.execute("INSERT INTO DATOSUSUARIOS VALUES(NULL,?,?,?,?,?)",(datos))

    miConexion.commit()

    messagebox.showinfo("BBDD","Registro Exitoso")

def leer():

    miConexion=sqlite3.connect("Usuarios")

    miCursor=miConexion.cursor()

    miCursor.execute("SELECT * FROM DATOSUSUARIOS WHERE ID=" + miId.get())

    elUsuario=miCursor.fetchall()

    for usuario in elUsuario:

        miId.set(usuario[0])
        miNombre.set(usuario[1])
        miPass.set(usuario[2])
        miApellido.set(usuario[3])
        Midireccion.set(usuario[4])
        textoComentario.insert(1.0,usuario[5])

    miConexion.commit()

def actualizar():

    miConexion=sqlite3.connect("Usuarios")

    miCursor=miConexion.cursor()

    datos=miNombre.get(),miPass.get(),miApellido.get(),Midireccion.get(),textoComentario.get("1.0", END)

    miCursor.execute("UPDATE DATOSUSUARIOS SET NOMBRE_USUARIO=?,PASSWORD=?,APELLIDO=?,DIRECCION=?,COMENTARIOS=?" +
    "WHERE ID="+miId.get(),(datos))

    miConexion.commit()

    messagebox.showinfo("BBDD","Registro Actualizado con Exito")

def eliminar():

    miConexion=sqlite3.connect("Usuarios")

    miCursor=miConexion.cursor()

    miCursor.execute("DELETE FROM DATOSUSUARIOS WHERE ID="+ miId.get())

    miConexion.commit()

    messagebox.showinfo("BBDD", "El registro ha sido eliminado")

#---------------Insterfaz Grafica----------
principal=Tk()
# Cinta de las barras de menu
barraMenu=Menu(principal)
principal.config(menu=barraMenu, width=300, height=300)

bbddMenu=Menu(barraMenu,tearoff=0)
bbddMenu.add_command(label="conectar", command=conexionBBDD)
bbddMenu.add_command(label="Salir",command=salirAplicacion)

borrarMenu=Menu(barraMenu,tearoff=0)
borrarMenu.add_command(label="Borrar campos", command=limpiarDatos)

crudMenu=Menu(barraMenu,tearoff=0)
crudMenu.add_command(label="Crear",command=crear)
crudMenu.add_command(label="Leer",command=leer)
crudMenu.add_command(label="Actualizar",command=actualizar)
crudMenu.add_command(label="Borrar",command=eliminar)

ayudaMenu=Menu(barraMenu,tearoff=0)
ayudaMenu.add_command(label="Licencia")
ayudaMenu.add_command(label="Acerca de ...")

barraMenu.add_cascade(label="BBDD",menu=bbddMenu)
barraMenu.add_cascade(label="Borrar",menu=borrarMenu)
barraMenu.add_cascade(label="CRUD",menu=crudMenu)
barraMenu.add_cascade(label="Ayuda",menu=ayudaMenu)

#Comienzo de los campos

miFrame=Frame(principal)
miFrame.pack()

miId=StringVar()
miNombre=StringVar()
miApellido=StringVar()
miPass=StringVar()
Midireccion=StringVar()

cuadroID=Entry(miFrame,textvariable=miId)
cuadroID.grid(row=0, column=1,padx=10,pady=10)

cuadroNombre=Entry(miFrame,textvariable=miNombre)
cuadroNombre.grid(row=1, column=1,padx=10,pady=10)
cuadroNombre.config(fg="blue")

cuadroPass=Entry(miFrame,textvariable=miPass)
cuadroPass.grid(row=2, column=1,padx=10,pady=10)
cuadroPass.config(show="*")

cuadroApellido=Entry(miFrame,textvariable=miApellido)
cuadroApellido.grid(row=3, column=1,padx=10,pady=10)

cuadroDireccion=Entry(miFrame,textvariable=Midireccion)
cuadroDireccion.grid(row=4, column=1,padx=10,pady=10)

textoComentario=Text(miFrame, width=16, height=5)
textoComentario.grid(row=5,column=1,padx=10,pady=10)
scrollVert=Scrollbar(miFrame,command=textoComentario.yview)
scrollVert.grid(row=5,column=2,sticky="snew")
textoComentario.config(yscrollcommand=scrollVert.set)

#--------Labels(Etiquetas)

idLabel=Label(miFrame, text="ID:")
idLabel.grid(row=0,column=0,sticky="e", padx=10, pady=10)

nombreLabel=Label(miFrame, text="Nombre:")
nombreLabel.grid(row=1,column=0,sticky="e", padx=10, pady=10)

passLabel=Label(miFrame, text="Pass:")
passLabel.grid(row=2,column=0,sticky="e", padx=10, pady=10)

apellidoLabel=Label(miFrame, text="Apellido:")
apellidoLabel.grid(row=3,column=0,sticky="e", padx=10, pady=10)

direccionLabel=Label(miFrame, text="Direccion:")
direccionLabel.grid(row=4,column=0,sticky="e", padx=10, pady=10)

comentariosLabel=Label(miFrame, text="Comentarios:")
comentariosLabel.grid(row=5,column=0,sticky="e", padx=10, pady=10)

#--------Botones

miFrame2=Frame(principal)
miFrame2.pack()

botonCrear=Button(miFrame2,text="Crear",command=crear)
botonCrear.grid(row=1,column=0,sticky="e",padx=10,pady=10)

botonLeer=Button(miFrame2,text="Leer",command=leer)
botonLeer.grid(row=1,column=1,sticky="e",padx=10,pady=10)

botonActualizar=Button(miFrame2,text="Actualizar",command=actualizar)
botonActualizar.grid(row=1,column=2,sticky="e",padx=10,pady=10)

botonBorrar=Button(miFrame2,text="Borrar",command=eliminar)
botonBorrar.grid(row=1,column=3,sticky="e",padx=10,pady=10)


principal.mainloop()