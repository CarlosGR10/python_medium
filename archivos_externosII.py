#Dezplazar el puntero de los archivos
#Metodo seek, situa la posicion en los caracteres

from io import open

archivo_texto2=open("arch.txt","r") #Pener un archivo en modo lectura

archivo_texto2.seek(10) 

print(archivo_texto2.read()) #Realiza la lectura hasta donde seek indica

archivo_texto2.close()

#-----------------------------
archivo_texto2=open("arch.txt","r")

print(archivo_texto2.read(11))

print(archivo_texto2.read())

#Situar el puntero enmedio

archivo_texto2=open("arch.txt","r")

archivo_texto2.seek(len(archivo_texto2.read())/2) #situar en el puntero enmedio

print(archivo_texto2.read())

archivo_texto2.close()

#Abrir en modo lectura y escritura "r+"

archivo_texto2=open("arch.txt","r+")

archivo_texto2.write("Comienzo del texto") #Agrega un texto al inicio

archivo_texto2.close()

#Una linea en mitad del documento

archivo_texto2=open("arch.txt","r+")

print(archivo_texto2.readlines()) #-----> Muestra en la consola una lista de las lineas que tenemos

lista_de_texto=archivo_texto2.readlines(); # Crea un lista 

lista_de_texto[1]="Esta nine ha sido incluida \n"

archivo_texto2.writelines(lista_de_texto)

archivo_texto2.close()