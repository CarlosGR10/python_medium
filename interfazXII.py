from tkinter import *
from tkinter import messagebox # Ventanas emergentes

principal = Tk()

#------Ventanas emergentes

def infoAditional():
    messagebox.showinfo("Programa Cool", "Version 0.01")

def licencia():
    messagebox.showwarning("Licencia","La licencia es premium")

def salirAplicacion():
    valor = messagebox.askquestion("Salir","Desea salir de la aplicación")

    if valor == "yes":
        principal.destroy()

def cerrarAplicacion():
    valor = messagebox.askokcancel("Cerrar","Desea cerrar la aplicacion")
    
    if valor == True:
        principal.destroy()

def retrydocument():
    valor = messagebox.askretrycancel("Cerrar","La aplicacion se bloqueo, quiere reintentar")
    
    if valor == True:
        principal.destroy()
        
#----Menus

barraMenu=Menu(principal)
principal.config(menu=barraMenu)

archivoMenu=Menu(barraMenu, tearoff=0)
archivoMenu.add_command(label="Nuevo",font=("Arial",1))
archivoMenu.add_command(label="Abrir",font=("Arial",1))
archivoMenu.add_command(label="Guardar",font=("Arial",1))
archivoMenu.add_command(label="Guardar como",font=("Arial",1))
archivoMenu.add_separator()
archivoMenu.add_command(label="Cerrar",font=("Arial",1),command = retrydocument)
archivoMenu.add_command(label="Salir",font=("Arial",1),command = salirAplicacion)

archivoEdicion=Menu(barraMenu, tearoff=0)
archivoEdicion.add_command(label="Copiar",font=("Arial",1))
archivoEdicion.add_command(label="Pegar",font=("Arial",1))
archivoEdicion.add_command(label="Cortar",font=("Arial",1))

archivoTools=Menu(barraMenu,tearoff=0)

archivoHelp=Menu(barraMenu,tearoff=0)
archivoHelp.add_command(label="Acerca de...",font=("Arial",1),command=infoAditional)
archivoHelp.add_command(label="Licencia",font=("Arial",1),command=licencia)

barraMenu.add_cascade(label="Archivo",menu=archivoMenu,font=("Arial",1))
barraMenu.add_cascade(label="Edición",menu=archivoEdicion,font=("Arial",1))
barraMenu.add_cascade(label="Herramientas",menu=archivoTools,font=("Arial",1))
barraMenu.add_cascade(label="Ayuda",menu=archivoHelp,font=("Arial",1))

principal.mainloop()