#Aplicacion del Grid en las interfases

from tkinter import *

ventana=Tk()
ventana.geometry("700x600")

miFrame=Frame(ventana, width="100", height="100")
miFrame.pack()

#Se definen las entradas

cuadroNombre=Entry(miFrame)
cuadroNombre.grid(row=0, column=1)

cuadroApellido=Entry(miFrame)
cuadroApellido.grid(row=1, column=1)

cuadroDireccion=Entry(miFrame)
cuadroDireccion.grid(row=2, column=1)

# definimos las etiquetas de texto
# pad(x or y) --> sirve para la separacion de etiquetas (similar a  padding de css)
# sticky --> Es la posicion de las etiquetas (Basado en los puntos cardinales), n,s,e,w

cuadropass=Entry(miFrame)
cuadropass.grid(row=3, column=1)
cuadropass.config(show="*")

nombreLabel=Label(miFrame, text="Nombre")
nombreLabel.grid(row=0, column=0, pady=10, sticky="e")

apellidoLabel=Label(miFrame, text="Apellido")
apellidoLabel.grid(row=1, column=0, pady=10, sticky="e")

direccionLabel=Label(miFrame, text="Direccion")
direccionLabel.grid(row=2, column=0, pady=10, sticky="e")

passLabel=Label(miFrame, text="Password")
passLabel.grid(row=3, column=0, pady=10, sticky="e")


#Una forma mas sencilla de realizar 

""" labelNombre=Label(miFrame, text="Nombre:").grid(pady=5, row=0, column=0)
labelApellido=Label(miFrame, text="Apellido:").grid( pady=5, row=1, column=0)

prompNombre=Entry(miFrame, width=10).grid(padx=5, row=0, column=1)

prompApellido=Entry(miFrame, width=10).grid(padx=5, row=1, column=1) """

ventana.mainloop()
