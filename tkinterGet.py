#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 08:15:50 2019

@author: carlos
"""

from tkinter import *

def on_button():
    
    variable = datoAObtener.get() #Para obtener numero utilizar usar int/ float, Ejem. variable = float(datoAObtener.get())
    resultado.config(text = variable)

app = Tk()

elframe = Frame(app)
elframe.pack()

datoAObtener = Entry(elframe)
datoAObtener.grid(row=0,column=2,padx=10, pady=10)

button = Button(elframe,text="Btn Get", command=on_button)
button.grid(row=1,column=2,padx=10, pady=10)

resultado = Label(elframe, text="Dato obtener")
resultado.grid(row=2,column=2,padx=10, pady=10)

app.mainloop()