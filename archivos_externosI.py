#fases de los archivos externos
#1.-Cracion
#2.-Apertura
#3.-Manipulación
#4.-Cierre

# w - Modo escritura
# r - Modo lectura
# a - Agregar lineas

from io import open

archivo_texto=open("archivo.txt","w")  #Open da dos argumentos (Nombre del alchivo.Extención, Modo de abrirlo)

frase="Hola mundo, estamos estudiando Python" #Escribe en el archivo

archivo_texto.write(frase) 

archivo_texto.close() #El metodo close() cierra el proceso

#---------------->> Es para leer el texto dentro de un archivo
archivo_texto=open("archivo.txt","r")

texto=archivo_texto.read()

archivo_texto.close()

#---------------->>Convertir textos en listas

archivo_texto=open("archivo.txt","r")

lineas_texto=archivo_texto.readlines() #Concierte las lineas de texto en listas []

archivo_texto.close()

print(lineas_texto[0]) # Accede a los elementos de la lista 

#--------------->> Agregar lineas al archivo

archivo_texto=open("archivo.txt","a")

archivo_texto.write("\n Es bueno programar en python")

archivo_texto.close()