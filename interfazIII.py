#Creación de labels (Etiquetas)

from tkinter import *

vprincipal=Tk()
vprincipal.config(bg="#478D94")
vprincipal.config(relief="sunken") #Forma de una ventana
vprincipal.geometry("500x500")

miframe=Frame(vprincipal, width=300, height=250)
miframe.pack()
#poner una imagen
miImagen=PhotoImage(file="info.png")
Label(miframe, image=miImagen).place(x=50,y=50)

#Creación del label
milabel= Label(miframe, text="Hola Mundo", fg="#7CC2FF",font=("Arial",15),bg="blue")
milabel.place(x=100,y=100) #Posiciona la etiqueta 

vprincipal=mainloop()