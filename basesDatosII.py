import sqlite3

miConexion=sqlite3.connect("PrimeraBase")
miCursor=miConexion.cursor()

#miCursor.execute("CREATE TABLE PRODUCTOS(NOMBRE VARCHAR(50),PRECIO INTEGER, SECCION VARCHAR(20))")
#miCursor.execute("INSERT INTO PRODUCTOS VALUES('BALÓN',15,'Deportes')")

"""Crear unlote de registros"""
variosProductos = [
    ("Camiseta",10,"Deportes"),
    ("Jarron",90,"Ceramica"),
    ("Camion",20,"Juguetes")
]

miCursor.executemany("INSERT INTO PRODUCTOS VALUES (?,?,?)",variosProductos)

""" Ejecucución de un query """

miCursor.execute("SELECT * FROM PRODUCTOS")

variosProductos=miCursor.fetchall() #trae todos los datos de la base

#print(variosProductos) ---->Imprime directamente

for product in variosProductos:
    print("Nombre: ",product[0],"| Sección: ",product[2]) #Muestra los datos de la lista

miConexion.commit() # Es confirmar los cambios
miConexion.close()
