from tkinter import *
#Agregando funcionalidad a la calculadora
#Escribir los numeros en pantalla
#comandos

ventana=Tk()

miFrame=Frame(ventana)
miFrame.config(bg="#B3E7FF")
miFrame.pack()

#----Pantalla----
numeroPantalla=StringVar()

pantalla=Entry(miFrame, textvariable=numeroPantalla)
pantalla.grid(row=1,column=1, padx=10, pady=10 , columnspan=4)
pantalla.config(bg="black",fg="#8AFFD6", justify="right")

#------>>> Pulsar botones

def numeroPulsado(num):

    numeroPantalla.set(numeroPantalla.get()+ num) # En la funcion creamos un parametro

""" #Ejemplo
def numeroPulsado():

    numeroPantalla.set(numeroPantalla.get()+ "4") # Set imprime solo una vez, pero con get se repite

boton4=Button(miFrame, text="4", width=3, bg="#FFFFFF",command=numeroPulsado("4"))
boton4.grid(row=3, column=1) """

#---primera fila de botones

boton7=Button(miFrame, text="7", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("7"))
boton7.grid(row=2, column=1)

boton8=Button(miFrame, text="8", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("8"))
boton8.grid(row=2, column=2)

boton9=Button(miFrame, text="9", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("9"))
boton9.grid(row=2, column=3)

botonDiv=Button(miFrame, text="/", width=3, bg="#FFD9FD")
botonDiv.grid(row=2, column=4)

#---Segunda fila de botones

boton4=Button(miFrame, text="4", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("4"))
boton4.grid(row=3, column=1)

boton5=Button(miFrame, text="5", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("5"))
boton5.grid(row=3, column=2)

boton6=Button(miFrame, text="6", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("6"))
boton6.grid(row=3, column=3)

botonMult=Button(miFrame, text="X", width=3, bg="#4FE1D0")
botonMult.grid(row=3, column=4)

#---Tercera fila de botones

boton1=Button(miFrame, text="1", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("1"))
boton1.grid(row=4, column=1)

boton2=Button(miFrame, text="2", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("2"))
boton2.grid(row=4, column=2)

boton3=Button(miFrame, text="3", width=3, bg="#FFFFFF",command=lambda:numeroPulsado("3"))
boton3.grid(row=4, column=3)

botonRest=Button(miFrame, text="-", width=3, bg="#FF798B")
botonRest.grid(row=4, column=4)

#---Cuarta fila de botones

boton0=Button(miFrame, text="0", width=3, bg="#FFFFFF", command=lambda:numeroPulsado("0"))
boton0.grid(row=5, column=1)

botonComa=Button(miFrame, text=",", width=3, bg="#FFFFFF",  command=lambda:numeroPulsado("."))
botonComa.grid(row=5, column=2)

botonIgual=Button(miFrame, text="=", width=3)
botonIgual.grid(row=5, column=3)

botonSum=Button(miFrame, text="+", width=3 , bg="#B2FFE6")
botonSum.grid(row=5, column=4)

ventana.mainloop()