# -*- coding: utf-8 -*-
"""
Created on Sun Nov 10 17:06:58 2019

@author: Carlos García
"""

#Obtener datos

from tkinter import *

raiz = Tk()

miframe=Frame(raiz)
miframe.pack()

miNombre=StringVar() #Declaramos el tipo de variable para un entry

cuadroNombre=Entry(miframe, textvariable=miNombre) #Declaramos la variable en el Entry
cuadroNombre.grid(row=0,column=1,padx=20,pady=20)

def codigoBoton():
    
    miNombre.set("Carlos") #Funcion set() estableser valor a una variable
                            #Funcion get() obtiene informacion de un Entry
botonEnviar=Button(miframe,text="Enviar",command=codigoBoton)
botonEnviar.grid(row=1,column=1)
    

raiz.mainloop()