from tkinter import *

principal=Tk()

varOpcion=IntVar()

def imprimir():
    #print(varOpcion.get())
    if varOpcion.get() == 1:
        etiqueta.config(text="Masculino")
    elif varOpcion.get() ==2:
        etiqueta.config(text="Femenino")
    else:
        etiqueta.config(text="Otro")

Label(principal,text="Genero").pack()

#Forma de declarar los Radiobuttons 
Radiobutton(principal,text="Masculino",variable=varOpcion,value=1,command=imprimir).pack() #Creamos variables a los Radiobutton
Radiobutton(principal,text="Femenino",variable=varOpcion,value=2,command=imprimir).pack()
Radiobutton(principal,text="OTRO",variable=varOpcion,value=3,command=imprimir).pack()

etiqueta = Label(principal)

etiqueta.pack()


principal.mainloop()