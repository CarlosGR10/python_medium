"""
Created on Wed Sep 18 09:29:26 2019

@author: carlos
"""
#Creación de frames 

from tkinter import *

raiz=Tk()

raiz.title("My first window")

#raiz.resizable(1,0) #Método--- resizable(Ancho, Alto)

raiz.geometry("700x600") #Medidas de la ventana

raiz.config(bg="#F5F5F5") # backgroung de la ventana
            
miFrame=Frame() #Es la creacion del frame

miFrame.pack(side="left" , anchor="n") #Aqui queda empaquetado el Frame a la ventana Raiz            
#El método pack sirve para redimencionar el Frame
            
miFrame.config(bg="blue") #bg (background)

miFrame.config(width=480,height=320)     

miFrame.config(bd=20) #Tamaño de borde

miFrame.config(relief="sunken") #Tipo de borde (groove,sunken)

miFrame.config(cursor="pirate") #Marca de un cursor

raiz.mainloop() #Es una ventana en ejecucuión