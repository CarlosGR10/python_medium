# Crear una base de datos
""" 1.- Abrir y crear conexion
    2.-Crear puntero(Cursor)
    3.-Ejecutar query(consulta)SQL
    4.-Manejar los resultados del Query (CRUD)
    5.-Cerrar el puntero
    6.-Cerrar la conexión """

import sqlite3

miConexion=sqlite3.connect("PrimeraBase")
miCursor=miConexion.cursor() #Creasión de un cursor

#miCursor.execute("CREATE TABLE PRODUCTOS(NOMBRE VARCHAR(50),PRECIO INTEGER, SECCION VARCHAR(20))")
miCursor.execute("INSERT INTO PRODUCTOS VALUES('BALÓN',15,'Deportes')")
miConexion.commit() # Es confirmar los cambios
miConexion.close()