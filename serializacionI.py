#Para el manejo de ficheros externos

#Paso 1 Craer el fichero
# wb - write binari
# rb - read binari

import pickle

lista_nombres=["Carlos","Maira","Edurdo","Isabel"]

fichero_binario=open("lista_nombres","wb") # Al crear el fichero es ("nombre del fichero","wb") 

pickle.dump(lista_nombres, fichero_binario) #Volcado de la lista con el metodo dump

fichero_binario.close() 

# En otro archivo.py , rescatar el fichero

import pickle

fichero=open("lista_nombres","rb")

lista=pickle.load(fichero) #el metodo load es cargar el archivo binario

print(lista) # Nos mostrara la información del fichero que creamos