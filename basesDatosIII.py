#Poner campos clave y automatizar los ID

import sqlite3

miConexion=sqlite3.connect("Gestion de productos") #Creación de la base de datos

miCursor=miConexion.cursor()
#El Campo clave se declara como PRIMARY KEY

miCursor.execute('''
    CREATE TABLE PRODUCTOS(
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        NOMBRE_ARTICULO VARCHAR(50),
        PRECIO INTEGER,
        SECCION VARCHAR(20))
''')

productos=[
    ("Pelota",20,"Juguetes"),
    ("Pantalon",15,"Ropa"),
    ("Desarmador",25,"Ferretería"),
    ("Jarrón",50,"Cerámica")
]



miCursor.executemany("INSERT INTO PRODUCTOS VALUES (NULL,?,?,?)",productos)
#utilicamos la palabra NULL para automatizar el ID, y lods demas campos de la tabla van con (?)

miConexion.commit()
miConexion.close()
