from tkinter import *

principal = Tk()

barraMenu=Menu(principal) #Cramos la cinta del menu
principal.config(menu=barraMenu)

archivoMenu=Menu(barraMenu, tearoff=0) #Tearoff corta las lineas
archivoMenu.add_command(label="Nuevo",font=("Arial",1)) #add_command es para agregar a la cinta del menu
archivoMenu.add_command(label="Abrir",font=("Arial",1))
archivoMenu.add_command(label="Guardar",font=("Arial",1))
archivoMenu.add_command(label="Guardar como",font=("Arial",1))
archivoMenu.add_separator()
archivoMenu.add_command(label="Salir",font=("Arial",1))

archivoEdicion=Menu(barraMenu, tearoff=0)
archivoEdicion.add_command(label="Copiar",font=("Arial",1))
archivoEdicion.add_command(label="Pegar",font=("Arial",1))
archivoEdicion.add_command(label="Cortar",font=("Arial",1))

archivoTools=Menu(barraMenu,tearoff=0)

archivoHelp=Menu(barraMenu,tearoff=0)
archivoHelp.add_command(label="Acerca del programa",font=("Arial",1))

barraMenu.add_cascade(label="Archivo",menu=archivoMenu,font=("Arial",1))
barraMenu.add_cascade(label="Edición",menu=archivoEdicion,font=("Arial",1))
barraMenu.add_cascade(label="Herramientas",menu=archivoTools,font=("Arial",1))
barraMenu.add_cascade(label="Ayuda",menu=archivoHelp,font=("Arial",1))

principal.mainloop()